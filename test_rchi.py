# -*- coding: utf-8 -*-
from __future__ import print_function, unicode_literals
import difflib
import unittest
import io
import rchi
import shutil


class TestRPMChangelogInformation(unittest.TestCase):
    def setUp(self):
        self.testfile = 'zarafa.spec'
        shutil.copyfile("{0}.old".format(self.testfile), self.testfile)

    def test_generate_changelog_line(self):
        observed, pkg_owner, date = rchi.gen_chline(self.testfile)
        expected = u"* {} {} ".format(date, pkg_owner.decode('utf8')) + \
            u"- 7.1.4-1.1.svn41394"
        self.assertEqual(expected, observed.decode('utf8'))

    def test_rewrite_spec_file(self):
        rchi.rewrite_spec_file(self.testfile)
        old = io.open("{0}.old".format(self.testfile),
                      encoding="utf8").readlines()
        new = io.open(self.testfile, encoding="utf8").readlines()
        diff = list(difflib.unified_diff(old, new))
        self.assertEqual(len(diff), 12)


if __name__ == "__main__":
    unittest.main()
