#!/usr/bin/python

import rpm
import argparse
import datetime
import shutil
import tempfile
import logging
logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',
                    level=logging.INFO)


def gen_chline(sfile):
    spec = rpm.spec(sfile)
    date = datetime.date.today().strftime("%a %b %d %Y")
    headers = spec.packages[0].header
    version = headers['Version']
    release = ".".join(headers['Release'].split(".")[:-1])
    packager = headers['Packager']
    newheader = "* %s %s - %s-%s" % (date, packager, version, release)
    logging.debug('newheader = {}'.format(newheader))
    return newheader, packager, date


def rewrite_spec_file(sfile):
    inF = file(sfile)
    outF = tempfile.NamedTemporaryFile(delete=False)

    for line in inF:
        line = line.rstrip()
        if line == "%changelog":
            print >> outF, line
            print >> outF, "{0}\n- \n".format(gen_chline(sfile)[0])
        else:
            print >> outF, line

    outF.close()
    shutil.copyfile(outF.name, sfile)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate changelog entry.')
    parser.add_argument('filename', metavar='FILE')
    parser.add_argument('-l', '--line-only',
                        dest="line_only", action="store_true")
    args = parser.parse_args()

    if args.line_only:
        print(gen_chline(args.filename))
    else:
        rewrite_spec_file(args.filename)
